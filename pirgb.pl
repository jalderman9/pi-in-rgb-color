#!/usr/bin/perl
#
# Read in 1,000,000 digits of pi and convert to raw rgb image data,
# then convert to bmp if imagemagick is installed.

use warnings;
use strict;

my $pifile = 'pi1000000_nodec.txt';
my $output = 'pi.rgb';
my $scalefactor = 2.575757576; # We want to scale values from 0 to 99 in decimal to hex 0x00-0xFF

open(PI, "<", $pifile) || die "\nError: failed to open file for reading. \n $! \n";
open (OUT, ">:raw", $output) || die "\nError: failed to open output file for writing. \n $! \n";

# Read characters one at a time from the file, grouping digits into RGB colors
my $offset = 0;
while ($offset < 1000000)
{
	my ($r, $g, $b, $rh, $gh, $bh);
	my $bytesread;
	
	seek(PI, $offset, 0);
	$bytesread = read(PI, $r, 2, 0) || die "failed to read from file: $!";
	$offset += $bytesread;
	$rh = sprintf("%02x", int($r * $scalefactor));
	
	seek(PI, $offset, 0);
	$bytesread = read(PI, $g, 2, 0) || die "failed to read from file: $!";
	$offset += $bytesread;
	$gh = sprintf("%02x", int($g * $scalefactor));
	
	seek(PI, $offset, 0);
	$bytesread = read(PI, $b, 2, 0) || die "failed to read from file: $!";
	$offset += $bytesread;
	$bh = sprintf("%02x", int($b * $scalefactor));
	
	#print "Offset ".sprintf("%07d",$offset).": decimal $r $g $b, color = $rh $gh $bh\n";
	my $hexstr = $rh.$gh.$bh;
	my $packed = pack('H*', $hexstr);
	print OUT $packed;
}

close(PI);
close(OUT);

print "\nDone writing binary data.\n";

if (-e "/usr/bin/convert")
{
	print "Converting raw rgb to bmp...\n";
	`convert -depth 8 -size 408x408 pi.rgb -scale 200% pi.bmp`;
	print "Finished\n";
}
else
{
	print "You can convert to bmp with imagemagick:\n";
	print "  convert -depth 8 -size 408x408 pi.rgb -scale 200% pi.bmp\n";
}
exit(0);
